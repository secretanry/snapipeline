from flask import Flask

app = Flask(__name__)


@app.route('/print', methods=['GET'])
def hello_world():
    return 'It is a simplest flask app!'


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
